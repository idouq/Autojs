"ui";
var 配置 = storages.create("DSJL");
var version = 6.2;   //版本号
ui.layout(
    <ScrollView>
        <vertical>
            <appbar>
                <toolbar id="bt" title="抖商精灵" />
            </appbar>
            <Switch id="autoService" text="无障碍服务" checked="{{auto.service != null}}" padding="8 8 8 8" textSize="15sp" />
            <horizontal>
                <text text="  抖音客户端：" gravity="left" />
                <spinner id="khd" entries="抖音短视频|抖音极速版" />
            </horizontal>
            <horizontal>
                <text text="  运行总时间：" gravity="left" />
                <input id="yhsc" hint="时间(分钟)" textSize="15sp" inputType="number" gravity="center" />
                <text text="    喜欢概率：" gravity="left" />
                <input id="xhgl" hint="喜欢概率" textSize="15sp" inputType="number" gravity="center" />
            </horizontal>
            <horizontal>
                <text text="  浏览作品时间：" gravity="left" />
                <input id="jg" hint="最小值" textSize="15sp" inputType="number" gravity="center" />
                <text text="----" gravity="center" />
                <input id="jg1" hint="最大值" textSize="15sp" inputType="number" gravity="center" />
                <text text="   秒" gravity="left" />
            </horizontal>
            <horizontal>
                <text text="  作品关键词：" gravity="left" />
                <input id="key" hint="务必填写，不填写无法运行！" textSize="15sp" />
            </horizontal>
            <text text="   温馨提示：" gravity="left" />
            <text text="   1.软件根据关键词识别作品是否浏览；" gravity="left" />
            <text text="   2.关键词尽量精准，才能使账号更加垂直；" gravity="left" />
            <text text="   3.可设置多个关键词，分隔符“,”；" gravity="left" />
            <text text="   4.例如：剪辑,影视,观影,60帧等；" gravity="left" />
            <text text="   5.关键词尽可能多而精准；" gravity="left" />
            <horizontal>
                <text text="   系统提示：" gravity="left" />
                <checkbox id="支持" text="本软件无需root权限。" checked="true" />
            </horizontal>

            <horizontal>
                <text id="ver" text="   当前软件版本：  " gravity="left" />
                <text id="安卓版本" textSize="15sp" />
            </horizontal>
            <horizontal>
                <button id="start" text="开始运行" layout_weight="1" style="Widget.AppCompat.Button.Colored" />
            </horizontal>

        </vertical>
    </ScrollView>
);
ui.安卓版本.text("   本机安卓版本：" + device.release + "");
ui.ver.text("   当前软件版本：" + version + "     ");
ui.bt.title = "抖商精灵 V" + version;
ui.autoService.on("check", function (checked) {
    if (checked && auto.service == null) {
        app.startActivity({
            action: "android.settings.ACCESSIBILITY_SETTINGS"
        });
    }
    if (!checked && auto.service != null) {
        auto.service.disableSelf();
    }
});




var 悬块 = function (window, view) {
    if (!window || !view) {
        throw "缺参数";
    };
    this.x = 0, this.y = 0;
    this.windowX, this.windowY;
    this.downTime = 500;
    this.Timeout = 0;
    this.Click = function () { };
    this.LongClick = function () { };
    this.setClick = function (fun) {
        //判断参数类型是否为函数？
        if (typeof fun == "function") {
            this.Click = fun;
        };
    };
    this.setLongClick = function (fun, ji) {
        //判断参数类型是否为函数？
        if (typeof fun == "function") {
            this.LongClick = fun;
            //判断参数是否可为设置数字？
            if (parseInt(ji) <= 1000) {
                this.downTime = parseInt(ji);
            };
        };
    };

    view.setOnTouchListener(new android.view.View.OnTouchListener((view, event) => {
        switch (event.getAction()) {
            //按下事件。
            case event.ACTION_DOWN:
                //按下记录各种坐标数据。
                this.x = event.getRawX();
                this.y = event.getRawY();
                this.windowX = window.getX();
                this.windowY = window.getY();
                //创建一个定时器用来定时执行长按操作。
                this.Timeout = setTimeout(() => {
                    this.LongClick();
                    this.Timeout = 0;
                }, this.downTime);
                return true;
            //移动事件。
            case event.ACTION_MOVE:
                //移动距离过大则判断为移动状态
                // if (Math.abs(event.getRawY() - this.y) > 5 && Math.abs(event.getRawX() - this.x) > 5) {
                //     //移动状态清除定时器
                //     if (this.Timeout) {
                //         //定时器存在则清除定时器。
                //         clearTimeout(this.Timeout);
                //         this.Timeout = 0;
                //     };
                //     //移动手指时调整悬浮窗位置
                //     window.setPosition(this.windowX + (event.getRawX() - this.x), this.windowY + (event.getRawY() - this.y));
                // };
                return true;
            //抬起事件。
            case event.ACTION_UP:
                if (this.Timeout) {
                    clearTimeout(this.Timeout);
                    this.Timeout = 0;
                    this.Click();
                };
                return true;
        };
        return true;
    }));
};
var startime = Date.parse(new Date());
var 点赞数量 = 0;
var 浏览数量 = 0;
readconfig();//读取配置

ui.start.on("click", function () {
    //程序开始运行之前判断无障碍服务
    if (auto.service == null) {
        toast("请先开启无障碍服务！");
        return;
    }
    saveconfig();//保存配置
    xf();//悬浮窗
    抖音();//主线程
});



function 抖音() {
    主线程 = threads.start(function () {
        console.setTitle("抖商");
        console.show();
        ts("执行操作：脚本开始运行");
        gjcjc();
        返回首页();
        sleep(1000);
        _养号()
    })
}
function xf() {
    var thread = threads.start(function op() {
        var window = floaty.window(
            // <button id="but" w="35" h="60" text="停止" />
            <button id="but" text="stop" />
            // <img id="but" src="file://stop1.png" />
        );
        window.setPosition(10, device.height / 3)
        setInterval(() => { }, 500);
        var ad = new 悬块(window, window.but);
        ad.setLongClick(function () {
            var yxtime = (Date.parse(new Date()) - startime) / 1000;
            yxtime = formatSeconds(yxtime);
            log("提示：用户停止运行");
            log("时间提示：运行" + yxtime);
            log("浏览作品：" + 浏览数量);
            log("点赞作品：" + 点赞数量);
            exit();
        });
        ad.setClick(function () {
            var yxtime = (Date.parse(new Date()) - startime) / 1000;
            yxtime = formatSeconds(yxtime);
            log("提示：用户停止运行");
            log("时间提示：运行" + yxtime);
            log("浏览作品：" + 浏览数量);
            log("点赞作品：" + 点赞数量);
            exit();
        })
    })
    //创建一个新的悬浮控制模块 ad 并带入参数(所要控制的悬浮窗和用来控制悬浮窗移动的控件)。

}
function gjcjc() {
    var keyword = ui.key.text();
    var str = keyword.split(',');
    if (str.length <= 1) {
        var str = keyword.split('，');
    }
    log("关键词数量：" + str.length);
    for (k = 0; k < str.length; k++) {
        log("关键词" + (k + 1) + ":" + str[k]);
    }
}
function saveconfig() {
    配置.put("xhgl", ui.xhgl.text());
    配置.put("jg", ui.jg.text());
    配置.put("jg1", ui.jg1.text());
    配置.put("key", ui.key.text());
    配置.put("yhsc", ui.yhsc.text());
    配置.put("支持", ui.支持.checked);
    配置.put("khd", ui.khd.getSelectedItemPosition());
}
function readconfig() {
    ui.xhgl.text(配置.get("xhgl", "60"));
    ui.jg.text(配置.get("jg", "20"));
    ui.jg1.text(配置.get("jg1", "45"));
    ui.yhsc.text(配置.get("yhsc", "30"));
    ui.支持.checked = Boolean(配置.get("支持", true));
    ui.key.text(配置.get("key", ""));
    if (parseInt(配置.get("khd", ""))) {
        ui.khd.setSelection(parseInt(配置.get("khd", "")));
    };
}
function _视频检测() {
    var jstime = new Date().getTime();
    var h = device.height;
    var keyword = ui.key.text();
    if (!text("推荐").visibleToUser().exists()) {
        返回首页();
    }
    // if (text("评论并转发").visibleToUser().exists()) {
    //     back();
    //     sleep(1000);
    // }
    var uc = textMatches(".....*").longClickable().visibleToUser().boundsInside(0, device.height / 2, device.width, device.height).find();
    for (i = 0; i < uc.length; i++) {
        if (uc[i].text() == "点击进入直播间") {
            log("直播中，自动跳过");
            log("检测耗时：" + (new Date().getTime() - jstime) + "毫秒");
            return false;
        }
        if (uc[i].text().indexOf("[t]") != -1 || uc[i].text().indexOf("广告") != -1) {
            log("广告作品，自动跳过");
            log("检测耗时：" + (new Date().getTime() - jstime) + "毫秒");
            return false;
        }
    }

    var str = keyword.split(',');
    if (str.length <= 1) {
        var str = keyword.split('，');
    }
    str = str.filter(function (s) {
        return s && s.trim();
    });
    for (i = 0; i < uc.length; i++) {
        for (k = 0; k < str.length; k++) {
            if (uc[i].text().indexOf(str[k]) != -1) {
                log("检测成功，视频符合");
                浏览数量 = 浏览数量 + 1;
                log(uc[i].text());
                log("关键词：" + str[k]);
                log("检测耗时：" + (new Date().getTime() - jstime) + "毫秒");
                return true;
            }
        }
    }
    log("跳过！当前视频不包含关键词");
    log("检测耗时：" + (new Date().getTime() - jstime) + "毫秒");
    if (new Date().getTime() - jstime < 1500) {
        sleep(random(500, 1500));
    }
    return false;
}
function _养号() {
    var W = device.width;
    var H = device.height;
    var mt = Date.parse(new Date());
    返回首页();
    if (配置.get("key", "") == "") {
        ts("系统提示：没有添加关键词。");
        ts("系统提示：请添加后重试！");
        exit();
    }
    while (true) {
        if (_视频检测()) {
            var sj = random(parseInt(ui.jg.text()), parseInt(ui.jg1.text()));
            if (!isNaN(sj)) {
                ts("浏览时间：" + sj + "秒");
                sleep(sj * 1000);
            } else {
                ts("浏览时间：15秒");
                sleep(15 * 1000);
            }
            var sjs = random(1, 100);
            if (parseInt(ui.xhgl.text()) > sjs) {
                click(W / 2, H / 2);
                sleep(50);
                click(W / 2, H / 2);
                sleep(1000);
                点赞数量 = 点赞数量 + 1;
                log("系统提示：点赞数量：" + 点赞数量);
                // if (sjs > 39) {
                //     评论();
                // }
            }
        };
        swipe(W / 2, H * 4 / 5, W / 2, H / 6, random(300, 1000));
        sleep(random(500, 2000));
        if (textMatches("编辑资料.*|.*更多主播").exists()) {
            返回首页();
        };
        if (!text("推荐").visibleToUser().exists()) {
            返回首页();
        }
        if (text("评论并转发").visibleToUser().exists()) {
            back();
            sleep(1000);
        }
        if (Date.parse(new Date()) - mt > 1000 * 60 * parseInt(配置.get("yhsc", ""))) {
            ts("系统提示：养号结束。");
            var yxtime = (Date.parse(new Date()) - startime) / 1000;
            yxtime = formatSeconds(yxtime);
            log("时间提示：运行" + yxtime);
            log("浏览作品：" + 浏览数量);
            log("点赞作品：" + 点赞数量);
            return;
        }
        _弹窗();
        sleep(100);
    }
};
function _弹窗() {
    if (text("我知道了").exists()) {
        text("我知道了").findOne().click();
    }
    if (text("允许").exists()) {
        text("允许").findOne().click();
    }
    if (text("取消").exists()) {
        text("取消").findOne().click();
    }
    if (text("以后再说").exists()) {
        text("以后再说").findOne().click();
    }
    if (text("继续播放").exists()) {
        text("继续播放").findOne().click();
    }
    if (text("好的").exists()) {
        text("好的").findOne().click();
    }
}
function _判断是否登录() {
    返回首页();
    while (true) {
        _弹窗()
        if (text("我").exists()) {
            var b = text("我").findOne().bounds();
            if (b) {
                click(b.centerX(), b.centerY());
                sleep(1000);
            };
        };
        if (text("编辑资料").exists() || text("分享主页").exists() || text("编辑企业资料").exists()) {
            ts("操作提示：账号已登录")
            return true;
        };
        if (text("密码登录").exists()) {
            ts("操作提示：账号未登录");
            return false;
        };
        if (text("您的收藏在这里").exists()) {
            back();
            sleep(1000);
        };
        sleep(100);
    }
};
function 返回首页() {
    if (parseInt(配置.get("khd", "")) == 1) {
        app.startActivity({
            action: "android.intent.action.VIEW",
            data: "snssdk2329://feed"
        });
    } else {
        app.startActivity({
            action: "android.intent.action.VIEW",
            data: "snssdk1128://feed"
        });
    }
    sleep(100);
}
function ts(text) {
    log(text)
}
function formatSeconds(value) {
    var secondTime = parseInt(value);// 秒
    var minuteTime = 0;// 分
    var hourTime = 0;// 小时
    if (secondTime > 60) {
        minuteTime = parseInt(secondTime / 60);
        secondTime = parseInt(secondTime % 60);
        if (minuteTime > 60) {
            hourTime = parseInt(minuteTime / 60);
            minuteTime = parseInt(minuteTime % 60);
        }
    }
    var result = "" + parseInt(secondTime) + "秒";

    if (minuteTime > 0) {
        result = "" + parseInt(minuteTime) + "分" + result;
    }
    if (hourTime > 0) {
        result = "" + parseInt(hourTime) + "小时" + result;
    }
    return result;
}
function download(url, filePath) {
    importClass('java.io.FileOutputStream');
    importClass('java.io.IOException');
    importClass('java.io.InputStream');
    importClass('java.net.MalformedURLException');
    importClass('java.net.URL');
    importClass('java.net.URLConnection');
    importClass('java.util.ArrayList');

    var url = new URL(url);
    var conn = url.openConnection(); //URLConnection
    var inStream = conn.getInputStream(); //InputStream
    var fs = new FileOutputStream(filePath); //FileOutputStream
    var connLength = conn.getContentLength(); //int
    var buffer = util.java.array('byte', 1024); //byte[]
    var byteSum = 0; //总共读取的文件大小
    var byteRead; //每次读取的byte数
    // log('要下载的文件大小=');
    // log(connLength);
    var threadId = threads.start(function () {
        while (1) {
            var 当前写入的文件大小 = byteSum;
            var progress = (当前写入的文件大小 / connLength) * 100;
            if (progress > 0.1) {
                //var progress = parseInt(progress).toString() + '%';
                // ui.run(function () {
                //     w.progressNum.setText(progress);
                // });
                if (当前写入的文件大小 >= connLength) {
                    break;
                }
            }
            sleep(1000);
        }
    });
    while ((byteRead = inStream.read(buffer)) != -1) {
        byteSum += byteRead;
        //当前时间
        currentTime = java.lang.System.currentTimeMillis();
        fs.write(buffer, 0, byteRead); //读取
    }
    threadId && threadId.isAlive() && threadId.interrupt();
}
function 评论() {
    if (descStartsWith("评论").exists()) {
        var uc = descStartsWith("评论").find();
        for (i = 0; i < uc.length; i++) {
            if (uc[i].bounds().centerX() > 0 && uc[i].bounds().centerY() > 0 && uc[i].bounds().centerY() < device.height) {
                uc[i].click();
                //click(uc[i].bounds().centerX(), uc[i].bounds().centerY())
                sleep(2000);
                break;
            }
        }
    }
    if (textStartsWith("留下你的精彩评论").exists()) {
        if (!desc("[呲牙]").exists()) {
            log("操作提示：浏览评论");
            var a = "";

            var uc1 = textMatches(".+分.*钟.*前.*|.*小时前").visibleToUser(true).findOne(1000)
            if (uc1) {
                a = uc1.text();
                a = a.replace(/[0-9]/ig, "");
                a = 取文本左边(a, "前");
            }
            var num = random(2, 4)
            for (i = 0; i < num; i++) {
                var sjs = random(2, 5);
                swipe(device.width / 2, device.height - 260, device.width / 2, device.height / sjs, random(500, 2000))
                var sj = random(3, 7);
                sleep(sj * 1000);
            }
            if (uc1) {
                if (textStartsWith("留下你的精彩评论").exists()) {
                    textStartsWith("留下你的精彩评论").findOne().click();
                    sleep(1000);
                    if (setText(a)) {
                        sleep(1000);
                    }
                    if (desc("表情").exists()) {
                        var td = desc("表情").findOne().bounds();
                        click(td.right + 40, td.centerY());
                        sleep(2000);
                        log("操作提示：评论内容：" + a);
                    }
                }
            }
            var num = random(2, 4)
            for (i = 0; i < num; i++) {
                var sjs = random(2, 5);
                swipe(device.width / 2, device.height - 260, device.width / 2, device.height / sjs, random(500, 2000))
                var sj = random(3, 7);
                sleep(sj * 1000);
            }
        }
        else {
            back();
            sleep(2000);
        }

        back();
        sleep(2000);
    }
    app.startActivity({
        action: "android.intent.action.VIEW",
        data: "snssdk1128://feed"
    });
    sleep(1000);
}
function 取文本左边(str, a)  //取文本的中间值
{
    var w1 = str.indexOf(a);
    if (w1 < 0) {
        return false;
    };
    return str.substr(0, w1 - a.length - 2);
}
function sjstr(str) //返回一个随机值,分隔符,
{
    if (str) {
        strnum = str.split(",");
        if (strnum.length <= 1) {
            var strnum = str.split('，');
        }
        var sjz = random(0, parseInt(strnum.length) - 1);
        return strnum[sjz];
    }
}