'ui';

importClass(android.view.MenuItem);
importClass(com.google.android.material.bottomnavigation.BottomNavigationView);
importClass(com.google.android.material.bottomnavigation.LabelVisibilityMode);

ui.layout(
  <relative>

    <appbar
      id='appbar'
      w='*' >

      <toolbar
        id='toolbar' />

    </appbar>

    <viewpager
      id='vp'
      layout_below='appbar'
      layout_above='bnv' >

      <text text='第一页' gravity='center' />

      <text text='第二页' gravity='center' />

      <text text='第三页' gravity='center' />

      <text text='第四页' gravity='center' />

    </viewpager>

    <com.google.android.material.bottomnavigation.BottomNavigationView
      id='bnv'
      h='56'
      layout_alignParentBottom='true'
      layout_centerHorizontal='true'
      bg='#FAFAFA' />

  </relative>
);

let bnv = ui.bnv;

bnv.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);//1 不开动画 0 动画
/** 添加item */
let menu = bnv.getMenu();

menu.add(0, 0, 0, "界面1").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
menu.getItem(0).setIcon(getResID('ic_person_black_48dp', 'drawable'));

menu.add(0, 1, 1, "界面2").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
menu.getItem(1).setIcon(getResID('ic_person_black_48dp', 'drawable'));

menu.add(0, 2, 2, "界面3").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
menu.getItem(2).setIcon(getResID('ic_person_black_48dp', 'drawable'));

menu.add(0, 3, 3, "界面4").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
menu.getItem(3).setIcon(getResID('ic_person_black_48dp', 'drawable'));


/** ViewPager 与  BottomNavigationView 联动*/

/** 监听BottomNavigationView切换选中事件  */
bnv.setOnNavigationItemSelectedListener({
  onNavigationItemSelected: function (item) {
    //跳转到对应的页面
    ui.vp.setCurrentItem(item.getItemId());
    return true;
  }
});

// 为ViewPager添加页面改变事件
ui.vp.addOnPageChangeListener({
  onPageSelected: function (position) {
    // 将当前的页面对应的底部标签设为选中状态
    menu.getItem(position).setChecked(true);
    // 设置ToolBar标题
    ui.toolbar.setTitle(menu.getItem(position).getTitle());
  }
});

// 初始化 设置ToolBar标题
ui.toolbar.setTitle(menu.getItem(ui.vp.getCurrentItem()).getTitle());

/**
 * 获取资源ID
 * @param {*} name 
 * @param {*} type 
 */
function getResID(name, type) {
  return context.getResources().getIdentifier(name, type, context.getPackageName());
}