"ui";
var 配置 = storages.create("douzbdai");
var version = 5.0;
qdui();

function qdui() {
    ui.layout(
        <ScrollView>
            <vertical>
                <appbar>
                    <horizontal>
                        <toolbar id="bt" title="直播间留言" gravity="center" textColor="red" layout_weight="1" />
                        {/* <Switch id="openwin" text="悬浮窗" checked="{{open.win != null}}" padding="20 8 8 8" textSize="15sp" /> */}
                        <Switch id="autoService" text="无障碍" checked="{{auto.service != null}}" padding="20 20 8 8" textSize="15sp" layout_gravity="right" />
                    </horizontal>
                </appbar>
                <horizontal >
                    <text text="   打字速度：" gravity="left" />
                    <spinner id="khd" entries="快速|正常|慢速" />
                    <checkbox id="dengdai" text="进入直播间等待" checked="true" />
                </horizontal>
                <horizontal>
                    <text text="   关注主播：" gravity="left" />
                    <checkbox id="gzzb" text="开启关注主播" checked="true" />
                </horizontal>


                <horizontal>
                    <text text="   表情设置：" gravity="left" />
                    <checkbox id="sjbq" text="随机后缀表情" checked="true" />
                </horizontal>
                <radiogroup orientation="horizontal" >
                    <text text="   发送方式：" gravity="left" />
                    <radio id="sxfs" text="顺序发送      " />
                    <radio id="sjfs" text="随机发送" />
                </radiogroup>
                <horizontal>
                    <text text="   发送等待：" gravity="left" />
                    <input id="jg" hint="最小值" textSize="15sp" inputType="number" gravity="center" />
                    <text text="----" gravity="center" />
                    <input id="jg1" hint="最大值" textSize="15sp" inputType="number" gravity="center" />
                    <text text="   秒" gravity="left" />
                </horizontal>
                <horizontal>
                    <text text="   直播间人数限制小于：" gravity="left" />
                    <input id="renshu" hint="      " textSize="15sp" />
                    <text text="人" gravity="left" />
                </horizontal>
                <horizontal>
                    <text text="   每个直播间留言次数：" gravity="left" />
                    <input id="cishu" hint="      " textSize="15sp" />
                    <text text="次(不能大于20次)" gravity="left" />
                </horizontal>
                <horizontal>
                    <text text="   留言内容：" gravity="left" />
                    <input id="key" hint="务必填写，不填写无法留言！" textSize="15sp" />
                </horizontal>

                <text text="   温馨提示：" gravity="left" textColor="red" />
                <text text="   1.仅供学习，请勿用于商业用途。" gravity="left" />
                <text text="   2.QQ57466335。" gravity="left" />
                <button id="start" text="开始运行" style="Widget.AppCompat.Button.Colored" />
            </vertical>
        </ScrollView>
    );

    ui.autoService.on("check", function (checked) {
        if (checked && auto.service == null) {
            app.startActivity({
                action: "android.settings.ACCESSIBILITY_SETTINGS"
            });
        }
        if (!checked && auto.service != null) {
            auto.service.disableSelf();
        }
    });



    ui.emitter.on("resume", function () {
        ui.autoService.checked = auto.service != null;
    });


    readconfig();



    var 悬块 = function (window, view) {
        if (!window || !view) {
            throw "缺参数";
        };
        this.x = 0, this.y = 0;
        this.windowX, this.windowY;
        this.downTime = 500;
        this.Timeout = 0;
        this.Click = function () { };
        this.LongClick = function () { };
        this.setClick = function (fun) {
            //判断参数类型是否为函数？
            if (typeof fun == "function") {
                this.Click = fun;
            };
        };
        this.setLongClick = function (fun, ji) {
            //判断参数类型是否为函数？
            if (typeof fun == "function") {
                this.LongClick = fun;
                //判断参数是否可为设置数字？
                if (parseInt(ji) <= 1000) {
                    this.downTime = parseInt(ji);
                };
            };
        };

        view.setOnTouchListener(new android.view.View.OnTouchListener((view, event) => {
            switch (event.getAction()) {
                //按下事件。
                case event.ACTION_DOWN:
                    //按下记录各种坐标数据。
                    this.x = event.getRawX();
                    this.y = event.getRawY();
                    this.windowX = window.getX();
                    this.windowY = window.getY();
                    //创建一个定时器用来定时执行长按操作。
                    this.Timeout = setTimeout(() => {
                        this.LongClick();
                        this.Timeout = 0;
                    }, this.downTime);
                    return true;
                //移动事件。
                case event.ACTION_MOVE:
                    //移动距离过大则判断为移动状态
                    // if (Math.abs(event.getRawY() - this.y) > 5 && Math.abs(event.getRawX() - this.x) > 5) {
                    //     //移动状态清除定时器
                    //     if (this.Timeout) {
                    //         //定时器存在则清除定时器。
                    //         clearTimeout(this.Timeout);
                    //         this.Timeout = 0;
                    //     };
                    //     //移动手指时调整悬浮窗位置
                    //     window.setPosition(this.windowX + (event.getRawX() - this.x), this.windowY + (event.getRawY() - this.y));
                    // };
                    return true;
                //抬起事件。
                case event.ACTION_UP:
                    if (this.Timeout) {
                        clearTimeout(this.Timeout);
                        this.Timeout = 0;
                        this.Click();
                    };
                    return true;
            };
            return true;
        }));
    };
    function xf() {
        var thread = threads.start(function op() {
            var window = floaty.window(
                <img id="but" src="file://stop.png" />
            );
            window.setPosition(10, device.height / 3)
            setInterval(() => { }, 500);
            var ad = new 悬块(window, window.but);
            ad.setLongClick(function () {
                var yxtime = (Date.parse(new Date()) - startime) / 1000;
                yxtime = formatSeconds(yxtime);
                log("提示：用户停止运行");
                log("时间提示：运行" + yxtime);
                log("浏览作品：" + 浏览数量);
                log("点赞作品：" + 点赞数量);
                exit();
            });
            ad.setClick(function () {
                var yxtime = (Date.parse(new Date()) - startime) / 1000;
                yxtime = formatSeconds(yxtime);
                log("提示：用户停止运行");
                log("时间提示：运行" + yxtime);
                log("浏览作品：" + 浏览数量);
                log("点赞作品：" + 点赞数量);
                exit();
            })
        })
        //创建一个新的悬浮控制模块 ad 并带入参数(所要控制的悬浮窗和用来控制悬浮窗移动的控件)。

    }

    var startime = Date.parse(new Date());
    var 点赞数量 = 0;
    var 浏览数量 = 0;
    var uid = "";
    ui.start.on("click", function () {
        //程序开始运行之前判断无障碍服务
        if (auto.service == null) {
            toast("请先开启无障碍服务！");
            return;
        }
        saveconfig();
        xf();
        抖音();
    });
}

function 抖音() {
    主线程 = threads.start(function 抖音ll() {
        console.setTitle("内部版本");
        console.show();
        ts("执行操作：脚本开始运行");
        if (ui.cishu.text() > 20) {
            log("重要提示：次数设置超限！");
            exit();
        }
        app.launchApp("抖音短视频");
        sleep(3000);
        直播间();
    })
}
function gjcjc() {
    var keyword = ui.key.text();
    var str = keyword.split(',');
    if (str.length <= 1) {
        var str = keyword.split('，');
    }
    log("关键词数量：" + str.length);
    for (k = 0; k < str.length; k++) {
        log("关键词" + (k + 1) + ":" + str[k]);
    };
}
function _弹窗() {
    if (text("我知道了").exists()) {
        text("我知道了").findOne().click();
    }
    if (text("允许").exists()) {
        text("允许").findOne().click();
    }
    if (text("取消").exists()) {
        text("取消").findOne().click();
    }
    if (text("以后再说").exists()) {
        text("以后再说").findOne().click();
    }
    if (text("继续播放").exists()) {
        text("继续播放").findOne().click();
    } if (text("立即赠送").exists()) {
        text("立即赠送").findOne().click();
    }
}
function 返回首页() {
    app.startActivity({
        action: "android.intent.action.VIEW",
        data: "snssdk1128://feed"
    });
    sleep(100);
}
function ts(text) {
    log(text);
}
function 取文本中间(str, a, b)  //取文本的中间值
{
    var w1 = str.indexOf(a);
    if (w1 < 0) {
        return false;
    };
    w1 = w1 + a.length;
    w2 = str.indexOf(b, w1);
    if (w2 <= 0) {
        return false;
    }
    return str.substr(w1, w2 - w1);
}
function formatSeconds(value) {
    var secondTime = parseInt(value);// 秒
    var minuteTime = 0;// 分
    var hourTime = 0;// 小时
    if (secondTime > 60) {
        minuteTime = parseInt(secondTime / 60);
        secondTime = parseInt(secondTime % 60);
        if (minuteTime > 60) {
            hourTime = parseInt(minuteTime / 60);
            minuteTime = parseInt(minuteTime % 60);
        }
    }
    var result = "" + parseInt(secondTime) + "秒";

    if (minuteTime > 0) {
        result = "" + parseInt(minuteTime) + "分" + result;
    }
    if (hourTime > 0) {
        result = "" + parseInt(hourTime) + "小时" + result;
    }
    return result;
}
function 直播间() {
    var deng = ui.dengdai.checked;
    var gz = ui.gzzb.checked;
    var wz = 0;
    var wx = -1;
    var nr = "";
    var str = "";
    var cishu = 0;
    var W = device.width;
    var H = device.height;
    while (true) {
        if (textMatches("小时榜|小时榜.*|说点什么.*|.*本场点赞").exists()) {
            var uc = textMatches("[0-9]*").visibleToUser().clickable().boundsInside(device.width * 0.75, 0, device.width, device.height / 2).find();
            for (i = 0; i < uc.length; i++) {
                // if (uc[i].text() > ui.renshu.text()) {
                //     log("人数检测：人数超限切换");
                //     sml_move(W / 2 + random(-100, 100), H * 3 / 4 + random(-100, 10), W / 2 + random(-100, 100), H / 6 + random(-50, 100), random(300, 800));
                //     sleep(2000);
                //     cishu = 0;
                // }
                // else {
                if (deng) {
                    var sji = random(15, 40);
                    ts("系统提示：进直播间随机等待" + sji + "秒");
                    sleep(sji * 1000);
                    deng = false;
                }
                if (gz) {
                    var a = text("关注").visibleToUser().boundsInside(0, 0, device.width, device.height / 4).findOne(1000);
                    if (a) {
                        a.click();
                        sleep(2000);
                        log("系统提示：关注主播");
                        gz = false;
                    }
                }
                if (ui.tap.checked) {
                    log("主播点赞：随机次数点赞")
                    var sj = random(3, 7);
                    for (i = 0; i < sj; i++) {
                        var sj2 = random(3, 5);
                        for (i = 0; i < sj2; i++) {
                            click(device.width / 2 + random(-100, 100), device.height / 2 + random(-100, 100));
                            sleep(20);
                            click(device.width / 2 + random(-100, 100), device.height / 2 + random(-100, 100));
                            sleep(20);
                            click(device.width / 2 + random(-100, 100), device.height / 2 + random(-100, 100));
                            sleep(50);
                        }
                    }
                }

                str = ui.key.text();
                if (ui.sjfs.checked) {
                    str = sjstr(str);
                } else {
                    if (str) {
                        var strnum = str.split(",");
                        if (strnum.length <= 1) {
                            strnum = str.split('，');
                        }
                        strnum = strnum.filter(function (s) {
                            return s && s.trim();
                        });
                        if (wz <= strnum.length - 1) {
                            str = strnum[wz];
                            wz = wz + 1;
                        } else {
                            wz = 0;
                            str = strnum[wz];
                        }
                    }
                }
                var uc = textMatches("说点什么..*").find();
                for (i = 0; i < uc.length; i++) {
                    click(uc[i].bounds().centerX(), uc[i].bounds().centerY())
                    sleep(random(500, 3000));
                    break;
                }
                if (ui.sjbq.checked) {
                    let sjs = random(1, 100);
                    if (sjs >= 50) {
                        str = str + sjbq1();
                    }
                }
                inputtxt(str);
                sleep(random(500, 1000));
                var a = desc("发送").className("android.widget.Button").visibleToUser().findOne(1000);
                if (a) {
                    a.click();
                    sleep(random(500, 1000));
                    ts("发送内容：" + str);
                    cishu = cishu + 1;
                    var sj = random(parseInt(ui.jg.text()), parseInt(ui.jg1.text()));
                    sleep(1000);
                    ts("等待时间：" + sj + "秒");
                    sleep(sj * 1000);
                }
                var uc = textMatches("去购买|去抢购|去看看").visibleToUser().find();
                if (uc.length >= 1) {
                    back();
                    sleep(2000);
                }
                if (textMatches("立即购买.*|提交订单.*").exists()) {
                    back();
                    sleep(2000);
                }
                if (text("立即赠送").visibleToUser().exists()) {
                    sleep(random(500, 1000));
                    back();
                }
                if (cishu >= ui.cishu.text()) {
                    log("次数检测：次数达到切换直播间");
                    sml_move(W / 2 + random(-100, 100), H * 3 / 4 + random(-100, 10), W / 2 + random(-100, 100), H / 6 + random(-50, 100), random(300, 800));
                    sleep(2000);
                    cishu = 0;
                }
                // }
                break;
            }
        } else {
            var a = desc("发送").className("android.widget.Button").visibleToUser().findOne(1000);
            if (a) {
                a.click();
                sleep(random(500, 1000));
            }
            var uc = className("android.widget.EditText").visibleToUser().find();
            if (uc.length > 0) {
                back();
                sleep(2000);
            }
            uc = textMatches("去购买|去抢购|去看看").visibleToUser().find();
            if (uc.length >= 1) {
                back();
                sleep(2000);
            }
            if (textMatches("立即购买.*|提交订单.*").exists()) {
                back();
                sleep(2000);
            }
            if (text("立即赠送").visibleToUser().exists()) {
                sleep(random(500, 1000));
                back();
            }
            if (!textMatches("小时榜|小时榜.*|说点什么.*|.*本场点赞").exists()) {
                log("系统提示：请进入直播间！");
            }
            sleep(2000);
        }
    }
}
function bezier_curves(cp, t) {
    cx = 3.0 * (cp[1].x - cp[0].x);
    bx = 3.0 * (cp[2].x - cp[1].x) - cx;
    ax = cp[3].x - cp[0].x - cx - bx;
    cy = 3.0 * (cp[1].y - cp[0].y);
    by = 3.0 * (cp[2].y - cp[1].y) - cy;
    ay = cp[3].y - cp[0].y - cy - by;
    tSquared = t * t;
    tCubed = tSquared * t;
    result = {
        "x": 0,
        "y": 0
    };
    result.x = (ax * tCubed) + (bx * tSquared) + (cx * t) + cp[0].x;
    result.y = (ay * tCubed) + (by * tSquared) + (cy * t) + cp[0].y;
    return result;
};
//仿真随机带曲线滑动  
//qx, qy, zx, zy, time 代表起点x,起点y,终点x,终点y,过程耗时单位毫秒
function sml_move(qx, qy, zx, zy, time) {
    var xxy = [time];
    var point = [];
    var dx0 = {
        "x": qx,
        "y": qy
    };

    var dx1 = {
        "x": random(qx - 100, qx + 100),
        "y": random(qy, qy + 50)
    };
    var dx2 = {
        "x": random(zx - 100, zx + 100),
        "y": random(zy, zy + 50),
    };
    var dx3 = {
        "x": zx,
        "y": zy
    };
    for (var i = 0; i < 4; i++) {

        eval("point.push(dx" + i + ")");

    };
    // log(point[3].x)

    for (let i = 0; i < 1; i += 0.08) {
        xxyy = [parseInt(bezier_curves(point, i).x), parseInt(bezier_curves(point, i).y)]

        xxy.push(xxyy);

    }

    // log(xxy);
    gesture.apply(null, xxy);
};
function pinlv() {
    if (ui.tappl.getSelectedItemPosition() == 0) {
        sleep(random(500, 1500));
    }
    if (ui.tappl.getSelectedItemPosition() == 1) {
        sleep(random(1000, 3000));
    }
    if (ui.tappl.getSelectedItemPosition() == 2) {
        sleep(random(2500, 5000));
    }
}
function djcs() {
    if (ui.djsc.getSelectedItemPosition() == 0) {
        return random(5, 10);
    }
    if (ui.djsc.getSelectedItemPosition() == 1) {
        return random(3, 7);
    }
    if (ui.djsc.getSelectedItemPosition() == 2) {
        return random(2, 4);
    }
}
function inputtxt(str) {
    var i = 0;
    while (true) {
        setText(str.substr(0, i));
        if (ui.khd.getSelectedItemPosition() == 0) {
            sleep(random(20, 100));
        }
        if (ui.khd.getSelectedItemPosition() == 1) {
            sleep(random(100, 500));
        }
        if (ui.khd.getSelectedItemPosition() == 2) {
            sleep(random(500, 1500));
        }
        i = i + 1;
        if (i > str.length) {
            break;
        }
    }

}
function sjstr(str) //返回一个随机值,分隔符,
{
    if (str) {
        strnum = str.split(",");
        if (strnum.length <= 1) {
            var strnum = str.split('，');
        }
        strnum = strnum.filter(function (s) {
            return s && s.trim();
        });
        var sjz = random(0, parseInt(strnum.length) - 1);
        return strnum[sjz];
    }
}
function sjbq1() //返回一个随机表情
{
    var str = "[微笑]|[惊呆]|[爱慕]|[流泪]|[可爱]|[害羞]|[呲牙]|[来看我]|[捂脸]|[耶]|[灵光一闪]|[偷笑]|[送心]|[大笑]|[泣不成声]|[可怜]|[奸笑]|[笑哭]|[黑线]|[坏笑]|[得意]|[翻白眼]|[互粉]|[吻]|[舔屏]|[飞吻]|[汗]|[擦汗]|[做鬼脸]|[尬笑]|[奋斗]|[惊喜]|[嘿哈]|[憨笑]|[囧]";
    if (str) {
        str = str.split("|");
        var sjz = random(0, parseInt(str.length) - 1);
        return str[sjz];
    }

}
function 取文本右边(str, a)  //取文本的中间值
{
    var w1 = str.indexOf(a);
    if (w1 < 0) {
        return false;
    };
    w1 = w1 + a.length;
    w2 = str.length;
    return str.substr(w1, w2 - w1);
}
function saveconfig() {
    配置.put("jg", ui.jg.text());
    配置.put("jg1", ui.jg1.text());
    配置.put("cishu", ui.cishu.text());
    配置.put("renshu", ui.renshu.text());
    配置.put("key", ui.key.text());
    配置.put("dengdai", ui.dengdai.checked);
    配置.put("khd", ui.khd.getSelectedItemPosition());
    配置.put("sjfs", ui.sjfs.checked);
    配置.put("sxfs", ui.sxfs.checked);
    配置.put("sjbq", ui.sjbq.checked);
    配置.put("gzzb", ui.gzzb.checked);
    // 配置.put("tap", ui.tap.checked);
}
function readconfig() {
    ui.jg.text(配置.get("jg", "2"));
    ui.jg1.text(配置.get("jg1", "5"));
    ui.key.text(配置.get("key", "榜一榜二带榜三，一看就知不简单,关注主播不迷路，主播带你上高速,万水千山总是情，点个关注行不行"));
    // ui.tap.checked = Boolean(配置.get("tap", ""));
    ui.dengdai.checked = Boolean(配置.get("dengdai", true));
    ui.khd.setSelection(parseInt(配置.get("khd", "1")));
    ui.sjfs.checked = Boolean(配置.get("sjfs", true));
    ui.sxfs.checked = Boolean(配置.get("sxfs", ""));
    ui.sjbq.checked = Boolean(配置.get("sjbq", true));
    ui.gzzb.checked = Boolean(配置.get("gzzb", ""));
    ui.cishu.text(配置.get("cishu", "10"));
    ui.renshu.text(配置.get("renshu", "500"));
}
