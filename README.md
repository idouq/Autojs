# Autojs

#### 声明
本仓库所有代码仅用于学习，不能用于其他用途。

#### 项目介绍
- 项目地址：https://github.com/hyb1996/Auto.js
- 官方论坛：https://www.autojs.org/
- 在线文档：https://hyb1996.github.io/AutoJs-Docs/#/
- 简介：Auto.JS是Android平台上的JavaScript自动化工具，一个支持无障碍服务的Android平台上的Javascript IDE,其发展目标是JsBox和Workflow，它的本质是可执行自己编写的简易Javascript脚本的，尤其可以在开启“无障碍模式”的情况下对其他App进行一些操作的一个Android App，便于进行自动化操作。
- 主要功能：由无障碍服务实现的简单易用的自动操作函数
- 协议：基于Mozilla Public License Version 2.0

#### 可以做的事情
1. 数据监控：可以监视当前手机的数据。
2. 图片监控：截图获取当前页面信息。
3. 控件操作：模拟操作手机控件。
4. 自动化工作流：编写简单的脚本，完成一系列自动化操作。如：微信自动点赞，快速抢单等。
5. 定时功能：定时执行某个脚本，来完成定时任务。如：定时打卡签到等。


#### 使用说明

1.  下载Autojs软件
2.  下载本仓库中源码，导入Autojs中
3.  找到源码点击运行即可


#### 特别说明

1.  本仓库所有代码仅用于学习，不能用于其他用途。
2.  本仓库部分源码来源于网络，因无法找到作者部分源码暂未标注作者。